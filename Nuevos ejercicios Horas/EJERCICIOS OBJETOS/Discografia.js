class Discografía {

    ListaDiscos=[];
   
    añadeDisco(disco)
    {
        this.ListaDiscos.push(disco);
    }
    borraDisco(atributo)
    {
        this.ListaDiscos.splice(atributo,1);
    }
    ordenaDisco(atributo)
    {
        this.Discos.sort((a,b) => {
            if (a[atributo] < b[atributo]) {
                return -1;
            }
            if (a[atributo] > b[atributo]) {
                return 1;
            } 
            else return 0;
        });
    }
    MostrarDisco()
    {
        var text="";
        for (const disco of this.ListaDiscos) {
            text += "<tr><td>" + disco.nombre + "</td><td>" +  disco.interprete.nombreArtistico +"</td><td>" +  disco.añoPublicacion + "</td><td>" +  disco.tipoMusica + "</td></tr>"
        }
        return text;
    }
}
