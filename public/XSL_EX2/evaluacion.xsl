<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <html>
    <body>
        <h1>M04 - Notas</h1>
        <table style="border: 1px solid blue">
            <tr style="background-color:lightblue">
                <th style="text-align:left">Nombre</th>
                <th style="text-align:left">Apellidos</th>
                <th style="text-align:left">Telefono</th>
                <th style="text-align:left">Repetidos</th>
                <th style="text-align:left">Nota Práctica</th>
                <th style="text-align:left">Nota Examen</th>
                <th style="text-align:left">Nota Total</th>
            </tr>
            <xsl:for-each select="evaluacion/alumno">
            <xsl:sort select="apellidos" order="ascending"/>
            <tr>
            <xsl:choose>
                <xsl:when test="img">
                    <th>
                        <xsl:element name="img">
                            <xsl:attribute name="src">avatar.png</xsl:attribute> <!--Debes de poner tu imagen.png propia-->
                            <xsl:attribute name="width">100</xsl:attribute>
                            <xsl:attribute name="height">100</xsl:attribute>
                        </xsl:element>
                    </th>
                </xsl:when>
                <xsl:otherwise>
                    <th>
                        <xsl:element name="img">
                            <xsl:attribute name="src">images.jpg</xsl:attribute> <!--Debes de poner tu imagen.png propia-->
                            <xsl:attribute name="width">100</xsl:attribute>
                            <xsl:attribute name="height">100</xsl:attribute>
                        </xsl:element>
                    </th>
                </xsl:otherwise>
            </xsl:choose>
                <th><xsl:value-of select="nombre"></xsl:value-of></th>
                <th><xsl:value-of select="apellidos"></xsl:value-of></th>
                <th><xsl:value-of select="telefono"></xsl:value-of></th>
                <th><xsl:value-of select="@repite"></xsl:value-of></th>
                <th><xsl:value-of select="notas/practicas"></xsl:value-of></th>
                <th><xsl:value-of select="notas/examen"></xsl:value-of></th>
                <xsl:if test="(notas/practicas+notas/examen)div 2 &lt; 5">
                    <th style="background-color:red"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                </xsl:if>
                <xsl:if test="(notas/practicas+notas/examen)div 2 &gt; 8">
                    <th style="background-color:blue"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                </xsl:if>
                <xsl:if test="(notas/practicas+notas/examen)div 2 &gt; 4 and (notas/practicas+notas/examen)div 2 &lt; 8">
                    <th style="background-color:green"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                </xsl:if>              
                <!--<th><xsl:value-of select="estado_cielo[@periodo='00-24']/@descripcion"></xsl:value-of></th>
                <th><img src="{concat('imagenes/',estado_cielo[@periodo='00-24']/@descripcion)}.png" style="width:100px"/></th>-->
            </tr>
            </xsl:for-each>
        </table>
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>
