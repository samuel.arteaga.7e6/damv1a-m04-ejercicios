<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <html>
    <body>
        <table style="border: 1px solid blue">
            <tr>
                <td>Fecha</td>
                <td>Maxima</td>
                <td>Minima</td>
                <td>Prediccion</td>
            </tr>
            <xsl:for-each select="root/prediccion/dia">
            <xsl:sort select="temperatura/maxima" order="descending"/>
            <tr>
                <td><xsl:value-of select="@fecha"></xsl:value-of></td>
                <td><xsl:value-of select="temperatura/maxima"></xsl:value-of></td>
                <td><xsl:value-of select="temperatura/minima"></xsl:value-of></td>
                <td><xsl:value-of select="estado_cielo/@descripcion"></xsl:value-of></td>

            </tr>
            </xsl:for-each>
        </table>
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>

