//Evento que se lanza al cerrar esta ventana y así avisamos al que nos ha abierto.
window.addEventListener("beforeunload", function () {
	window.opener.postMessage("ventana_cerrada", "*");
});
console.log("SI JS");
// Función para guardar la configuración y cerrar el modal
function saveConfig() {
	var backgroundColor = document.getElementById("background-color").value;
	var fontColor = document.getElementById("font-color").value;

	localStorage.setItem("background-color", backgroundColor);
	localStorage.setItem("font-color", fontColor);
	//Si activamos esta cookie solo entraremos la primera vez.
	localStorage.setItem("visited", true);
		
	window.close();
}

// Event listener para el submit del formulario de configuración
document.getElementById("config-form").addEventListener("submit", function (event) {
	event.preventDefault();
	saveConfig();
});