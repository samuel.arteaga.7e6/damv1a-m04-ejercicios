// Función para obtener el valor de una cookie
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}


// Función para mostrar el modal de configuración si es la primera vez que se visita la página
function showConfigModal() {
    var visited = getCookie("visited");
    if (!visited) {
        //abrimos ventana de configuración
        window.open('http://127.0.0.1:5500/UF3/Ejercicio%20Cookies/popUP.html', 'Config', 'toolbar=yes,statusbar=yes,width=600,height=400')

        //Especificamos el evento que nos indica que la ventana ha sido cerrada
        window.addEventListener("message", function (event) {
            if (event.data === "ventana_cerrada") {
                changeConfig();
            }
        });
    }else{changeConfig();}
}
showConfigModal();


function changeConfig() {
    console.log("Entra Config");
    // Aplicamos la configuración guardada en la cookie a la página principal
    var backgroundColor = getCookie("background-color");
    var fontColor = getCookie("font-color");
    if (backgroundColor) {
        document.body.style.backgroundColor = backgroundColor;
    }
    if (fontColor) {
        document.body.style.color = fontColor;
    }
}