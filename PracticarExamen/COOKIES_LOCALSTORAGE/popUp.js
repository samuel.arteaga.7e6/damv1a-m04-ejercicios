//Evento que se lanza al cerrar esta ventana y así avisamos al que nos ha abierto.
window.addEventListener("beforeunload", function () {
	window.opener.postMessage("ventana_cerrada", "*");
});

// Función para crear o actualizar una cookie
function setCookie(name, value, days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + (value || "") + expires + "; path=/";
	//document.cookie = name + "=" + value;

}


// Función para guardar la configuración y cerrar el modal
function saveConfig() {
	var backgroundColor = document.getElementById("background-color").value;
	var fontColor = document.getElementById("font-color").value;

	setCookie("background-color", backgroundColor, 1);
	setCookie("font-color", fontColor, 1);
	//Si activamos esta cookie solo entraremos la primera vez.
	//setCookie("visited", true, 1);
	//console.log(document.cookie);
	
	window.close();
}

// Event listener para el submit del formulario de configuración
document.getElementById("config-form").addEventListener("submit", function (event) {
	event.preventDefault();
	saveConfig();
});