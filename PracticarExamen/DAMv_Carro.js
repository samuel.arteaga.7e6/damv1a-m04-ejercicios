let productos = [{
    nombre: "Milkybar",
    foto: "milkybar.jpg",
    descripcion: "Chocolate blanco Nestlé Milkybar 100 g.",
    precio: 1.20,
    descuento: "10%",
}, {
    nombre: "Crunch",
    foto: "crunch.jpg",
    descripcion: "Chocolate crujiente Crunch Nestlé sin gluten 100 g.",
    precio: 1.32,
    descuento: "20%",
}, {
    nombre: "Croissants - La Bella Easo",
    foto: "croissant.jpg",
    descripcion: "Croissants 0% azucares La Bella Easo 360 g.",
    precio: 2.59,
    descuento: "",
}, {
    nombre: "Nesquik",
    foto: "nesquik.jpg",
    descripcion: "Chocolate con leche con relleno cremoso Nestlé Nesquik 100 g.",
    precio: 1.31,
    descuento: "30%",
}];

let user;
let carro;


function init() {
    recuperaDatosUsuario();
    muestraProductos();
}


function recuperaDatosUsuario() {
    /* Función para mostrar el modal de configuración si es la primera vez que se visita la página*/
}

function muestraProductos() {
    /** Función para crear los elementos necesarios para poder visualziar los productos. */
    for (let producto of productos) {

        let productos = document.getElementById("productos");

        let article = document.createElement("article");
        article.classList.add("articulo");
        productos.append(article);

        //PRiMER DIV
        let div = document.createElement("div");
        let primerp = document.createElement("p");
        primerp.classList.add("nombreW50");
        let mensaje = document.createTextNode(producto.nombre);
        primerp.append(mensaje);
        div.append(primerp);
        article.append(div);


        //CREO IMG
        let img = document.createElement("img")
        img.src = "src/milkybar.jpg";
        let verImagen = document.createTextNode(producto.foto);
        img.append(verImagen);
        article.append(img);

        //CREO SEGUNDO P
        let segundoP = document.createElement("p");
        let span = document.createElement("span");
        let verPrecio = document.createTextNode(producto.precio);
        span.append(verPrecio);
        segundoP.append(span);
        article.append(segundoP);

        //CREO TERCER P
        let tercerP = document.createElement("p");
        let verDescripcion = document.createTextNode(producto.descripcion);
        tercerP.append(verDescripcion);
        article.append(tercerP);

        //CREO CUARTO P
        let cuartoP = document.createElement("p");
        let input = document.createElement("input");

        input.type = "number";
        input.id = "Milkybar";
        input.step = "1";
        input.min = "0";
        let verDescuento = document.createTextNode(producto.descuento);
        input.append(verDescuento);
        cuartoP.append(input);
        article.append(cuartoP);
    }
}

function anadirProductos() {
    /** Función que muestra el ticket con los productos, precios y descuentos así como el total del precio de la compra. */
    let CompraArticulo = new Articulo()
    ;

}


function creaCelda(valor) {
    let celda = document.createElement("td");
    celda.innerText = valor;
    return celda;
}

function vaciaCarro() {
    let tabla = document.getElementById("tablebody");
    while (tabla.hasChildNodes()) {
        let child = tabla.removeChild(tabla.firstChild)

    }

}