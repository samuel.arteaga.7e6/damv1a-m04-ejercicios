<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <h2>VIDEOJUEGOS</h2>
        <xsl:for-each select="Videojuegos/Videojuego">
            <article>
                <img src="{concat('src/',foto)}"></img>
                <h3><xsl:value-of select="Nombre"></xsl:value-of></h3>
                <p><xsl:value-of select="Descripcion"></xsl:value-of></p>
                <div>
                    <xsl:attribute name="class">diva</xsl:attribute>
                    <a><xsl:attribute name="href"><xsl:value-of select="Link"/>
                    </xsl:attribute>más info<a>
                </div>
                <xsl:value-of select="Enlace"></xsl:value-of>
            </article>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
