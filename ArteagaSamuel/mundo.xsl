<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <html>
    <body>
        <h1>Africa</h1>
        <xsl:for-each select="mundo/continente">
            <table style="border: 1px solid blue">
                <tr>
                    <td>Bandera</td>
                    <td>País</td>
                    <td>Gobierno</td>
                    <td>Capital</td>
                </tr>
                <tr>
                <td><xsl:value-of select="paises/pais/foto"></xsl:value-of></td>
                <td><xsl:value-of select="paises/pais/capital"></xsl:value-of></td>
                <td><xsl:value-of select="paises/pais/nombre/gobierno/text()"></xsl:value-of></td>
                <td><xsl:value-of select="paises/pais"></xsl:value-of></td>
                </tr>
                <xsl:if test="paises/pais/gobierno[@dictaduras]">
                    <td style="background-color:red"><xsl:value-of select="paises/pais/gobierno[@dictaduras]"></xsl:value-of></td>
                </xsl:if>
                <xsl:if test="paises/pais/gobierno[@republica]">
                    <td style="background-color:yellow"><xsl:value-of select="paises/pais/gobierno[@republica]"></xsl:value-of></td>
                </xsl:if>
            </table>
        </xsl:for-each>
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>
