class Inmueble {
    direccion;
    nMetros;
    refCatastral;
    precio;
    foto;
    estado;
    coordenadas;
    edad;
    alta;
    visible;

    constructor(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad) {
        this.direccion = direccion;
        this.nMetros = nMetros;
        this.refCatastral = refCatastral;
        this.precio = precio;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas = coordenadas;
        this.edad = edad;
        this.alta = false;
        this.visible = false;
    }

    precioInmueble(precioOriginal) {
        if (this.edad > 10) {
            for (let i = 0; i < this.edad - 10 && i != 10; i++) {
                this.precio -= precioOriginal * 0.01;
            }
        }
    }

    darAlta() {
        this.alta = true;
    }

    darBaja() {
        this.alta = false;
    }

    activar() {
        this.visible = true;
    }

    desactivar() {
        this.visible = false;
    }
}