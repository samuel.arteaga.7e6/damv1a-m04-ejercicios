class Restauracion extends Local {
    extractora;
    cafetera;
    mobiliario;

    constructor(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, alta, visible, nVentanas, extractora, cafetera, mobiliario) {
        super(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, alta, visible, nVentanas, persiana, ventanal);

        this.extractora = extractora;
        this.cafetera = cafetera;
        this.mobiliario = mobiliario;
    }

    precioLocalRestauracion() {
        let precioOriginal = this.precio;
        super.precioLocal(precioOriginal);
    }
}