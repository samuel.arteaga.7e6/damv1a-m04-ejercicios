class Comercial extends Local {
    adaptado;

    constructor(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, nVentanas, persiana, ventanal, adaptado) {
        super(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, nVentanas, persiana, ventanal);

        this.adaptado = adaptado;
    }

    precioLocalComercial() {
        let precioOriginal = this.precio;
        super.precioLocal(precioOriginal);
    }
}