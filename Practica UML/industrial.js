class Industrial extends Local {
    puertos;
    suelo;

    constructor(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, nVentanas, persiana, ventanal, puertos, suelo) {
        super(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, nVentanas, persiana, ventanal);

        this.puertos = puertos;
        this.suelo = suelo;
    }

    precioLocalIndustrial() {
        let precioOriginal = this.precio;
        super.precioLocal(precioOriginal);

        if (this.suelo == "urbano") {
            this.precio += precioOriginal * 0.25;
        }
    }
}