class Inmueble{
    
    direccion;
    nMetros;
    #refCatastral;
    precio;
    foto;
    estado;
    coordenadas;
    edad;
    alta;
    visible;
    constructor(direccion, nMetros, precio, foto, estado, coordenadas, edad, alta, visible) 
    {
        this.direccion =direccion;
        this.nMetros = nMetros;
        this.#refCatastral= "";
        this.precio=precio;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas= coordenadas;
        this.edad= edad;
        this.alta= false;
        this.visible=visible;

    }
    verAntiguedad(){
       
    }
    darAlta(){
        
        this.alta=true;
        

    }
    darBaja(){
        this.alta=false;
        
    }
    modificar(){
        
    }
    activar(){
        this.visible=true;
    }
    desactivar(){
        this.visible=false;
    }
}
class Local extends Inmueble{
    nVentanas;
    persiana;
    ventanal;
    constructor(direccion, nMetros, precio, foto, estado, coordenadas, edad, alta, visible,nVentanas,persiana,ventanal){
        this.direccion =direccion;
        this.nMetros = nMetros;
        this.precio=precio;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas= coordenadas;
        this.edad= edad;
        this.alta=alta;
        this.visible=visible;
        this.nVentanas=nVentanas;
        this.persiana=persiana;
        this.ventanal=ventanal;
    }
    numeroMetros(nMetros,precio,ventanal){
        this.precio=precio;
        this.nMetros=nMetros;
        this.ventanal=ventanal;

        if(nMetros>50){
            precio = precio*0.01;
        }
    }
    tieneVentanal(precio,ventanal){
        this.precio=precio;
        this.ventanal=ventanal;

        if(ventanal<=1){
            precio= precio*0.02;
        }
    }
    numeroVentanas(precio,ventanal){
        this.precio=precio;
        this.ventanal=ventanal;

        if(ventanal>4){
            precio=precio/0.02;
        }
    }
}
class Comercial extends Local{
    adaptado;
    constructor(direccion, nMetros, precio, foto, estado, coordenadas, edad, alta, visible,adaptado){
        this.direccion =direccion;
        this.nMetros = nMetros;
        this.precio=precio;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas= coordenadas;
        this.edad= edad;
        this.alta=alta;
        this.visible=visible;
        this.adaptado=adaptado;
    }
}
class Industrial extends Local{
    puertos;
    suelo;
    constructor(direccion, nMetros, precio, foto, estado, coordenadas, edad, alta, visible,puertos,suelo){
        this.direccion =direccion;
        this.nMetros = nMetros;
        this.precio=precio;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas= coordenadas;
        this.edad= edad;
        this.alta=alta;
        this.visible=visible;
        this.puertos=puertos;
        this.suelo=suelo;
    }
    enSueloUrbano(puertos,suelo,precio){
        this.puertos=puertos;
        this.suelo=suelo;
        this.precio=precio;
        if(suelo=="urbano"){
            precio*=1.25;
        }     
    }
}
class Restauracion extends Local{
    extractora;
    cafetera;
    mobiliario;
    constructor(direccion, nMetros, precio, foto, estado, coordenadas, edad, alta, visible, extractora,cafetera,mobiliario){
        this.direccion =direccion;
        this.nMetros = nMetros;
        this.precio=precio;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas= coordenadas;
        this.edad= edad;
        this.alta=alta;
        this.visible=visible;
        this.extractora=extractora;
        this.cafetera=cafetera;
        this.mobiliario=mobiliario;
    }
}
class Casa{

}
class Piso{

}