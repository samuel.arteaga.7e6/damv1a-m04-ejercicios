// Clase base para todos los inmuebles
class Inmueble {
    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas) {
        this.direccion = direccion;
        this.metrosCuadrados = metrosCuadrados;
        this.referenciaCatastral = referenciaCatastral;
        this.precioBase = precioBase;
        this.foto = foto;
        this.estado = estado;
        this.coordenadas = coordenadas;
    }

    // Método para calcular el precio final del inmueble
    calcularPrecioFinal() {
        let precioFinal = this.precioBase;

        // Reducción del precio por antigüedad
        const antiguedad = new Date().getFullYear() - this.estado;
        if (antiguedad > 10) {
            precioFinal -= (antiguedad - 10) * (this.precioBase * 0.01);
        }

        return precioFinal;
    }
}

// Clase para los pisos
class Piso extends Inmueble {
    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas, piso, ascensor, terraza) {
        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas);
        this.piso = piso;
        this.ascensor = ascensor;
        this.terraza = terraza;
    }

    // Override del método de la clase base
    calcularPrecioFinal() {
        let precioFinal = super.calcularPrecioFinal();

        // Incremento de precio por piso tercero o superior con ascensor
        if (this.ascensor && this.piso >= 3) {
            precioFinal += this.precioBase * 0.03;
        }

        // Incremento de precio por terraza
        if (this.terraza) {
            precioFinal += this.metrosCuadrados * 300;
        }

        return precioFinal;
    }
}

// Clase para las casas
class Casa extends Inmueble {
    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas, tipo, numHabitaciones, numBanos, jardin, piscina) {
        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, estado, coordenadas);
        this.tipo = tipo;
        this.numHabitaciones = numHabitaciones;
        this.numBanos = numBanos;
        this.jardin = jardin;
        this.piscina = piscina;
    }

    // Override del método de la clase base
    calcularPrecioFinal() {
        let precioFinal = super.calcularPrecioFinal();

        // Incremento de precio por jardín
        if (this.jardin) {
            if (this.jardin > 250) {
                precioFinal += this.precioBase * 0.09;
            } else if (this.jardin > 100) {
                precioFinal += this.precioBase * 0.05;
            }

            // Incremento de precio por piscina
            if (this.piscina) {
                precioFinal += this.precioBase * 0.04;
            }
        }

        return precioFinal;
    }
}

