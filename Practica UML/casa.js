class Casa extends Inmueble {
    nHabitaciones;
    nBaños;
    tipo;
    familia;
    jardin;
    piscina;

    constructor(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, nHabitaciones, nBaños, tipo, familia, jardin, piscina) {
        super(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad);

        this.nHabitaciones = nHabitaciones;
        this.nBaños = nBaños;
        this.tipo = tipo;
        this.familia = familia;
        this.jardin = jardin;
        this.piscina = piscina;
    }

    precioCasa() {
        let precioOriginal = this.precio;
        super.precioInmueble(precioOriginal);

        if (this.jardin > 250) {
            this.precio += precioOriginal * 0.09;
        } else if (this.jardin > 100) {
            this.precio += precioOriginal * 0.05
        }

        if (this.piscina == true) {
            this.precio += precioOriginal * 0.04;
        }
    }
}