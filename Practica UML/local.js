class Local extends Inmueble {
    nVentanas;
    persiana;
    ventanal;

    constructor(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad, nVentanas, persiana, ventanal) {
        super(direccion, nMetros, refCatastral, precio, foto, estado, coordenadas, edad);

        this.nVentanas = nVentanas;
        this.persiana = persiana;
        this.ventanal = ventanal;
    }

    precioLocal(precioOriginal) {
        super.precioInmueble(precioOriginal);

        if (this.nMetros > 50) {
            this.precio += precioOriginal * 0.01;
        }

        if (this.nVentanas > 4) {
            this.precio += precioOriginal * 0.02;
        }

        if (this.ventanal <= 1) {
            this.precio -= precioOriginal * 0.02;
        }
    }
}